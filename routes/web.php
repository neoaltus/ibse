<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','PagesController@index')->name('index.index');
Route::get('/about','PagesController@about')->name('about.index');
Route::get('/contact','PagesController@contact')->name('contact.index');;
Route::get('/causes','PagesController@causes')->name('cause.index');;
Route::get('/donate','PagesController@donate')->name('donate.index');;
Route::get('/gallery','PagesController@gallery')->name('gallery.index');;
Route::get('/news','PagesController@news')->name('news.index');;
Route::get('/cause-single','PagesController@causeSingle');
Route::get('/single','PagesController@single')->name('single.index');
Route::get('/testimonal','PagesController@testimonal');


