<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
    	return view('charity.index');
    }
    public function about(){
    	return view('charity.about');
    }
    public function contact(){
    	return view('charity.contact');
    }
    public function causes(){
    	return view('charity.causes');
    }
    public function donate(){
    	return view('charity.donate');
    }
    public function gallery(){
    	return view('charity.gallery');
    }
    public function news(){
    	return view('charity.news');
    }
    public function single(){
        return view('charity.single');
    }
    public function causeSingle(){
    	return view('charity.cause-single');
    }
    public function testimonial(){
    	return view('charity.testimonial');
    }
}
