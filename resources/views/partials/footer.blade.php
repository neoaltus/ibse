<div class="container">
          <div class="row">
            <div class="col-md-4 probootstrap-animate">
              <div class="probootstrap-footer-widget">
                <h3>About Us</h3>
                <p>We are reach out to the people we come together and discuss problems affecting them, proffer solutions and implement those solutions</p>
                <ul class="probootstrap-footer-social">
                  <li><a href="#"><i class="icon-twitter"></i></a></li>
                  <li><a href="#"><i class="icon-facebook"></i></a></li>
                  <li><a href="#"><i class="icon-github"></i></a></li>
                  <li><a href="#"><i class="icon-dribbble"></i></a></li>
                  <li><a href="#"><i class="icon-linkedin"></i></a></li>
                  <li><a href="#"><i class="icon-youtube"></i></a></li>
                </ul>
              </div>
            </div>
           
            <div class="col-md-4 probootstrap-animate">
              <div class="probootstrap-footer-widget">
                <h3>Contact Info</h3>
                <ul class="probootstrap-contact-info">
                  <li><i class="icon-location2"></i> <span>No 275 utako, Abuja Nigeria. 07038294286</span></li>
                  <li><i class="icon-question"></i><span>For help and counselling call</span></li>
                  <li><i class="icon-phone2"></i><span>08020558444, 08114140026</span></li>
                </ul>
                
              </div>
            </div>

            <div class="col-md-4 probootstrap-animate">
              <div class="probootstrap-footer-widget">
                <h3>Donation</h3>
                <p>Making a difference by extending a helping hand for a social cause is like prestidigitating the magic of highest prestige</p>
                <p><a href="#" class="btn btn-primary">Donate Now</a></p>
              </div>
            </div>
           
          </div>
          <!-- END row -->
          
        </div>

        <div class="probootstrap-copyright">
          <div class="container">
            <div class="row">
              <div class="col-md-8 text-left">
                <p>&copy; {{date('Y')}} <a href="http://beta.ibse.org.ng/">IBSE</a>. All Rights Reserved. Designed &amp; Developed with <i class="icon icon-heart"></i> by <a href="http://beta.ibse.org.ng/">IBSE</a></p>
              </div>
              <div class="col-md-4 probootstrap-back-to-top">
                <p><a href="#" class="js-backtotop">Back to top <i class="icon-arrow-long-up"></i></a></p>
              </div>
            </div>
          </div>
        </div>