
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> IBSE</title>
    <meta name="description" content="a non-profit and non-political organization that will provide a platform for the people of Nigeria to come together and discuss problems affecting them, proffer and implement solutions. Facilitating community reorientation and sensitization programs on drug education, sale and abuse of illicit drugs. Introduce drug free clubs in both government and private schools in Nigeria. Encourage school enrollment and reduce the rate of dropouts in Nigeria. ">
    <meta name="keywords" content=" a non-profit and non-political organization that will provide a platform for the people of Nigeria  for drug abuse education
     Empower the communities by encouraging, community works, skills acquisition and training.">
    
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Montserrat:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/styles-merged.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
