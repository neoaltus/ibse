
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IBSE</title>
    <meta name="description" content="Initiative For Better Societal Enlightenment">
    <meta name="keywords" content="Initiative For Better Societal Enlightenment">
    
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Montserrat:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/styles-merged.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    
      <nav class="navbar navbar-default probootstrap-navbar">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html" title="uiCookies:Enlight">IBSE</a>
          </div>

          <div id="navbar-collapse" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="index.html">Home</a></li>
              <li><a href="about.html">About Us</a></li>
              <li><a href="news.html">News</a></li>
              <li><a href="causes.html">Causes</a></li>
              <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Pages</a>
                <ul class="dropdown-menu">
                  <li><a href="about.html">About Us</a></li>
                  <li><a href="testimonial.html">Testimonial</a></li>
                  <li><a href="cause-single.html">Cause Single</a></li>
                  <li><a href="gallery.html">Gallery</a></li>
                  <li class="dropdown-submenu dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><span>Sub Menu</span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Second Level Menu</a></li>
                      <li><a href="#">Second Level Menu</a></li>
                      <li><a href="#">Second Level Menu</a></li>
                      <li><a href="#">Second Level Menu</a></li>
                    </ul>
                  </li>
                  <li><a href="news.html">News</a></li>
                  <li><a href="contact.html">Contact</a></li>
                </ul>
              </li>
              
              <li class="probootstra-cta-button last"><a href="donate.html" class="btn btn-primary">Donate</a></li>
            </ul>
          </div>
        </div>
      </nav>
      
      <section class="probootstrap-hero" style="background-image: url({{asset('img/substance-abuse-consequences.jpg')}}"  data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="probootstrap-slider-text probootstrap-animate" data-animate-effect="fadeIn">
                <h1 class="probootstrap-heading probootstrap-animate">IBSE <span>...Together we can make a difference</span></h1>
                <p class="probootstrap-animate"><a href="#" class="btn btn-primary btn-lg">Donate Now</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="probootstrap-service-intro">
          <div class="container">
            <div class="probootstrap-service-intro-flex">
              
              <div class="item probootstrap-animate" data-animate-effect="fadeIn">
                <div class="icon">
                  <i class="icon-heart"></i>
                </div>
                <div class="text">
                  <h2>Become Volunteer</h2>
                  <p>Become a volunteer, join us during progrmas and events support our causes Online O foffline</p>
                  <p><a href="#">Learn More</a></p>
                </div>
              </div>
              <div class="item probootstrap-animate" data-animate-effect="fadeIn">
                <div class="icon">
                  <i class="icon-graduation-cap"></i>
                </div>
                <div class="text">
                  <h2>Progams</h2>
                  <p>We haveseveral programs primariy geared toward the reducing the scourgeof Substance abuse</p>
                  <p><a href="#">Learn More</a></p>
                </div>
              </div>
              <div class="item probootstrap-animate" data-animate-effect="fadeIn">
                <div class="icon">
                  <i class="icon-wallet"></i>
                </div>
                <div class="text">
                  <h2>Give Donation</h2>
                  <p>Donate to our causes by prvinding financial or material resources to support our efforts</p>
                  <p><a href="#">Lear More</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center section-heading probootstrap-animate" data-animate-effect="fadeIn">
              <h2>Some of our Causes</h2>
              <p class="lead">Sed a repudiandae impedit voluptate nam Deleniti dignissimos perspiciatis nostrum porro nesciunt</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate" data-animate-effect="fadeIn">
              <div class="probootstrap-image-text-block probootstrap-cause">
                <figure>
                  <img src="img/causes_3.jpg" alt="" class="img-responsive">
                </figure>
                <div class="probootstrap-cause-inner">
                  

                  
                  
                  <h2><a href="#">Government Engagement</a></h2>
                  <div class="probootstrap-date"><i class="icon-calendar"></i>Engaging Governments</div>  
                  
                  <p>AFABS at meeting of stakeholders with presidential advisory commitee on grugs</p>
                  <!-- <p><a href="#" class="btn btn-primary btn-black">Donate Now!</a></p> -->
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate" data-animate-effect="fadeIn">
              <div class="probootstrap-image-text-block  probootstrap-cause">
                <figure>
                  <img src="img/causes_2.jpg" alt="" class="img-responsive">
                </figure>
                <div class="probootstrap-cause-inner">
                  
                  
                  <h2><a href="#">Support For rehablitation Centres</a></h2>
                  <div class="probootstrap-date"><i class="icon-calendar"></i>Facilities Review </div>  
                  
                  <p>AFABS visting a rehablitation center for evaluation</p>
                <!--   <p><a href="#" class="btn btn-primary btn-black">Donate Now!</a></p> -->
                </div>
              </div>
            </div>
            <div class="clearfix visible-sm-block visible-xs-block"></div>
            <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate" data-animate-effect="fadeIn">
              <div class="probootstrap-image-text-block  probootstrap-cause">
                <figure>
                  <img src="img/causes_1.jpg" alt="" class="img-responsive">
                </figure>
                <div class="probootstrap-cause-inner">
                 
                  
                  <h2><a href="#">School Programe and Public Awarenes Campaigns</a></h2>
                  <div class="probootstrap-date"><i class="icon-calendar"></i> awarenes and sesitization </div>  
                  
                  <p>AFABS at  public awareness and Seisitization campaing</p><!-- 
                  <p><a href="#" class="btn btn-primary btn-black">Donate Now!</a></p> -->
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <!-- <p>Save the future for the little children by donating. <a href="causes.html">See all causes</a></p>
    -->         </div>
          </div>
        </div>
      </section>

      
      <section class="probootstrap-section probootstrap-bg probootstrap-section-dark" style="background-image: url({{asset('img/hero_bg_bw_1.jpg')}}"  data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center section-heading probootstrap-animate" data-animate-effect="fadeIn">
              <h2>Benefactor of our program</h2>
              <p class="lead">Identities of Ex-addicts/Recoverig addicts have been concealed for their safety.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3">
              <div class="probootstrap-donors text-center probootstrap-animate">
                <figure class="media">
                  <img src="img/person_6.jpg" alt="" class="img-responsive">
                </figure>
                <div class="text">
                  <h3>Segun Tunde</h3>
                  <p class="donated">they provided counselling and helped me quit my tramadol addiction.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="probootstrap-donors text-center probootstrap-animate">
                <figure class="media">
                  <img src="img/person_1.jpg" alt="" class="img-responsive">
                </figure>
                <div class="text">
                  <h3>Jamil Hussain</h3>
                  <p class="donated">Great organization, they helped me overcome my addiction</p>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="probootstrap-donors text-center probootstrap-animate">
                <figure class="media">
                  <img src="img/person_5.jpg" alt="" class="img-responsive">
                </figure>
                <div class="text">
                  <h3>Ogechukwu</h3>
                  <p class="donated">Their rehablitation centers are great and they encourage you to quit your addiction.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3">

              <div class="probootstrap-donors text-center probootstrap-animate">
                <figure class="media">
                  <img src="img/person_7.jpg" alt="" class="img-responsive">
                </figure>
                <div class="text">
                  <h3>Theresa </h3>
                  <p class="donated">They provided me with conselling and support during my recovery and aided me with support to stat my small trading business post recovery</p>
                </div>
              </div>  

            </div>
          </div>
        </div>
      </section>
      
      <section class="probootstrap-section  probootstrap-section-colored">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center section-heading probootstrap-animate">
              <h2>What People Says</h2>
              <p class="lead">Sed a repudiandae impedit voluptate nam Deleniti dignissimos perspiciatis nostrum porro nesciunt</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 probootstrap-animate">
              <div class="owl-carousel owl-carousel-testimony owl-carousel-fullwidth">
                <div class="item">

                  <div class="probootstrap-testimony-wrap text-center">
                    <figure>
                      <img src="img/person_1.jpg" alt="">
                    </figure>
                    <blockquote class="quote">&ldquo;Design must be functional and functionality must be translated into visual aesthetics, without any reliance on gimmicks that have to be explained.&rdquo; <cite class="author"> &mdash; <span>Mike Fisher</span></cite></blockquote>
                  </div>

                </div>
                <div class="item">
                  <div class="probootstrap-testimony-wrap text-center">
                    <figure>
                      <img src="img/person_2.jpg" alt="">
                    </figure>
                    <blockquote class="quote">&ldquo;Creativity is just connecting things. When you ask creative people how they did something, they feel a little guilty because they didn’t really do it, they just saw something. It seemed obvious to them after a while.&rdquo; <cite class="author"> &mdash;<span>Jorge Smith</span></cite></blockquote>
                  </div>
                </div>
                <div class="item">
                  <div class="probootstrap-testimony-wrap text-center">
                    <figure>
                      <img src="img/person_3.jpg" alt="">
                    </figure>
                    <blockquote class="quote">&ldquo;I think design would be better if designers were much more skeptical about its applications. If you believe in the potency of your craft, where you choose to dole it out is not something to take lightly.&rdquo; <cite class="author">&mdash; <span>Brandon White</span></cite></blockquote>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </section>
        
      <section class="probootstrap-half">
        <div class="image">
          <div class="image-bg">
            <img src="img/img_sq_5_big.jpg" alt="">
          </div>
        </div>
        <div class="text">
          <div class="probootstrap-animate">
            <h3>Sucess Stories</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt excepturi dicta ex, placeat ab esse, iure harum eaque fuga asperiores distinctio amet temporibus enim illum molestiae neque ad similique possimus repellendus velit! Quaerat nihil nemo, aliquam consectetur debitis illum. Excepturi cum, quaerat minus odit dolorem recusandae, debitis reprehenderit voluptate?</p>
            <p><a href="#" class="btn btn-primary btn-lg">Read More</a></p>
          </div>
        </div>
      </section>

      <section class="probootstrap-section">
        <div class="container">
          <div class="row">
            <div class="col-md-3 probootstrap-animate">
              <h3>News</h3>
              <ul class="probootstrap-news">
                <li>
                  <span class="probootstrap-date">July 16, 2017</span>
                  <h3><a href="#">Porro provident suscipit</a></h3>
                  <p>Provident sequi assumenda quaerat minima mollitia at ducimus atque aliquam a ad dolore.</p>
                  <p><span class="probootstrap-meta"><a href="#"><i class="icon-redo2"></i> 14</a> <a href="#"><i class="icon-bubbles2"></i> 7</a></span></p>
                </li>
                <li>
                  <span class="probootstrap-date">July 16, 2017</span>
                  <h3><a href="#">Voluptatem dolor pariatur</a></h3>
                  <p>Provident sequi assumenda quaerat minima mollitia at ducimus atque aliquam a ad dolore.</p>
                  <p><span class="probootstrap-meta"><a href="#"><i class="icon-redo2"></i> 14</a> <a href="#"><i class="icon-bubbles2"></i> 7</a></span></p>
                </li>
              </ul>
              <p><a href="#" class="btn btn-primary">View all news</a></p>
            </div>
            <div class="col-md-6 probootstrap-animate">
              <h3>About Us</h3>
              <p><img src="img/img_sm_2.jpg" alt="" class="img-responsive"></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem porro impedit, perferendis similique voluptatum reiciendis laudantium iusto ad, eligendi non minus nemo tempora? Non velit ab quasi at dignissimos fugiat.</p>
              <p><a href="#" class="btn btn-primary">Learn More</a></p>
            </div>
            <div class="col-md-3 probootstrap-animate">
              <h3>Gallery</h3>
              
              <div class="owl-carousel owl-carousel-fullwidth">
                <div class="item">
                  <a href="img/causes_1.jpg" class="image-popup">
                    <img src="img/causes_1.jpg" alt="" class="img-responsive">
                  </a>
                </div>
                <div class="item">
                  <a href="img/causes_2.jpg" class="image-popup">
                    <img src="img/causes_2.jpg" alt="" class="img-responsive">
                  </a>
                </div>
                <div class="item">
                  <a href="img/causes_3.jpg" class="image-popup">
                    <img src="img/causes_3.jpg" alt="" class="img-responsive">
                  </a>
                </div>
                
                
                
                
              </div>

              <p class="text-center"><a href="#" class="btn btn-primary">View all gallery</a></p>

            </div>
          </div>
        </div>
      </section>

      <footer class="probootstrap-footer probootstrap-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-4 probootstrap-animate">
              <div class="probootstrap-footer-widget">
                <h3>About Us</h3>
                <p>We aim to facilitate community reorientation and sensitization programs on drug education, sale and abuse of illicit drugs and aleviating extreme poverty by reducing level of unemployment through special economic schemes.</p>
                <ul class="probootstrap-footer-social">
                  <li><a href="#"><i class="icon-twitter"></i></a></li>
                  <li><a href="#"><i class="icon-facebook"></i></a></li>
                  <li><a href="#"><i class="icon-github"></i></a></li>
                  <li><a href="#"><i class="icon-dribbble"></i></a></li>
                  <li><a href="#"><i class="icon-linkedin"></i></a></li>
                  <li><a href="#"><i class="icon-youtube"></i></a></li>
                </ul>
              </div>
            </div>
           
            <div class="col-md-4 probootstrap-animate">
              <div class="probootstrap-footer-widget">
                <h3>Contact Info</h3>
                <ul class="probootstrap-contact-info">
                  <li><i class="icon-location2"></i> <span>198 Utaco Abuja</span></li>
                  <li><i class="icon-mail"></i><span>info@ibse.com</span></li>
                  <li><i class="icon-phone2"></i><span>+234-703-211-5676</span></li>
                </ul>
                
              </div>
            </div>

            <div class="col-md-4 probootstrap-animate">
              <div class="probootstrap-footer-widget">
                <h3>Donation</h3>
                <p>Donate to our causes by provinding financial or material resources to support our efforts. for more information contact Us Info@ibse.org </p>
                <p><a href="#" class="btn btn-primary">Donate Now</a></p>
              </div>
            </div>
           
          </div>
          <!-- END row -->
          
        </div>

        <div class="probootstrap-copyright">
          <div class="container">
            <div class="row">
              <div class="col-md-8 text-left">
                <p>&copy; 2019 <a href="/">uiCookies:Charity</a>. All Rights Reserved. Designed &amp; Developed with <i class="icon icon-heart"></i> by <a href="/">uicookies.com</a></p>
              </div>
              <div class="col-md-4 probootstrap-back-to-top">
                <p><a href="#" class="js-backtotop">Back to top <i class="icon-arrow-long-up"></i></a></p>
              </div>
            </div>
          </div>
        </div>
      </footer>

    <script src="{{asset('js/scripts.min.js')}}"></script>
    <script src="{{asset('js/main.min.js')}}"></script>
    <script src="{{asset('js/custom.js')}}"></script>
    
  </body>
</html>

