<!DOCTYPE html>

<html lang="en">
<head>
@include('partials.head')

</head>
  <body>
    <nav class="navbar navbar-default probootstrap-navbar">
@include('partials.nav')
      </nav>
@yield('content')
     <footer class="probootstrap-footer probootstrap-bg">
        @include('partials.footer')
      </footer>
<!-- script -->
    @include('partials.script')

    <!--/ script -->
    
  </body>
</html>