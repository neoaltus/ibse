@extends('layout.main')  
@section('content')       
      <section class="probootstrap-hero probootstrap-hero-inner" style="background-image: url(img/hero_bg_bw_1.jpg)"  data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="probootstrap-slider-text probootstrap-animate" data-animate-effect="fadeIn">
                <h1 class="probootstrap-heading probootstrap-animate">Learn More About Drug Abuse<span>Together we can make a difference</span></h1>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section">
        <div class="container">
        

        

          <div class="row mb40">
            <div class="col-md-12 probootstrap-animate">
              <p><img src="img/new2.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></p>
            </div>
            <div class="col-md-12  news-entry probootstrap-animate">
              <h2 class="mb0"><a href="#">Alcohol (depressant)</a></h2>
              <p class="probootstrap-news-date">July 20, 2017 by Admin</p>
              <p>
                  The term “alcohol”
                  refers to substances made by fermenting or distilling grains or fruits; for
                  example, wine, beer and hard liquor. Alcohol is the most popular drug in most
                  provinces of Canada. short-term effects may
                  include depresses the central
                  nervous system (CNS) relaxation and
                  feelings of well-being, but in some cases depression and anger increased activity and decreased inhibition drowsiness,
                  dizziness, slurred speech and loss of coordination and concentration in extreme cases, loss of consciousness and even death long-term effects may include damage to the stomach lining, ulcers, decreased appetite and malnutrition liver damage, cirrhosis of the liver blackouts (periods when the person is conscious and functioning but is later unable to recall what they did or said) brain damage that causes problems with memory, judgment and abstract thinkingincreased risk ofhemorrhagic stroke impotence in men and
                  infertility in women cancer of the mouth
                  and throat, as well as other types of cancer heavy use can lead to
                  dependence. 
                  
                   <p>Tobacco (stimulant) is usually smoked but can also be chewed. short-term effects may include stimulates the central nervous system (CNS) feelings of pleasure, stimulation and relaxation increased heartbeat,
                  blood pressure and blood sugar irritations of the
                  throat and lungs (coughing) reduced appetite and
                  endurance long-term effects may include heart disease,
                  strokes, emphysema, chronic bronchitis and aneurysms cancer of the lung,
                  throat, stomach,bladder, kidney and pancreas physical and
                  psychological addiction with significant withdrawal symptoms 
                  Cannabis (marijuana) is the most widely used illegal drug in Canada. It is a mood-altering drug that comes from the Cannabis sativa plant. People use it in three forms: marijuana (the dried and ground bud of the plant), hash or hashish oil/weed oil. The cannabis high comes from the chemical THC. The cannabis that was smoked in the sixties and seventies was much less potent than it is today. With the improvement in horticulture and growing techniques, the concentration of THC in cannabis today
                  has increased substantially. short-term effects may include feelings of calmness and relaxation, as well as clumsiness and slowed-down reactions, drowsiness  giddiness, talkativeness or quiet seriousness heightened senses forgetfulness and reduced ability to concentrate distorted sense of
                  space and time increased heart rate and changes in blood pressure increased appetite anxiety, occasionally panic attacks and/ or paranoia (suspicious feelings) long-term effects may include: decreased motivation and interest, as well as difficulties with memory and concentration chronic coughing and lung infections cancer (As a result
                  of the heavy use of pesticides and fertilizer in indoor grow-ops, the tar in
                  cannabis smoke contains much higher amounts of cancer-producing agents
                  than tar in tobacco smoke.)  psychological and
                  physical dependence can occur among heavy or regular users.</p> 
            
            
            </p>
              <p><span class="probootstrap-meta-share"><a href="#"><i class="icon-redo2"></i> 14</a> <a href="#"><i class="icon-bubbles2"></i> 7</a></span> </p>
            </div>
          </div>

      

          

       


        </div>
      </section>

@endsection      

