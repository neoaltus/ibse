@extends('layout.main')
    
@section('content')     
     <section class="probootstrap-hero" style="background-image: url(img/hero_bg_bw_1.jpg)"  data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="probootstrap-slider-text probootstrap-animate" data-animate-effect="fadeIn">
                <h1 class="probootstrap-heading probootstrap-animate">Donate <span>We give cheerfully and accept gratefully</span></h1>
                <p class="probootstrap-animate"><a href="#" class="btn btn-primary btn-lg">Donate Now</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="probootstrap-service-intro">
          <div class="container">
            <div class="probootstrap-service-intro-flex">
              <div class="item probootstrap-animate" data-animate-effect="fadeIn">
                <div class="icon">
                  <i class="icon-wallet"></i>
                </div>
                <div class="text">
                  <h2>Give Donation</h2>
                  <p>Making donation is the ultimate sign of solidarity, Actions speak louder than words</p>
                  <p><a href="#">Lear More</a></p>
                </div>
              </div>
              <div class="item probootstrap-animate" data-animate-effect="fadeIn">
                <div class="icon">
                  <i class="icon-heart"></i>
                </div>
                <div class="text">
                  <h2>Become Volunteer</h2>
                  <p>Never doubt what a small group of thoughtful committed citizen change </p>
                  <p><a href="#">Learn More</a></p>
                </div>
              </div>
              <div class="item probootstrap-animate" data-animate-effect="fadeIn">
                <div class="icon">
                  <i class="icon-graduation-cap"></i>
                </div>
                <div class="text">
                  <h2>Give Scholarship</h2>
                  <p>An investment in knowledge pays the best interest help the children get food</p>
                  <p><a href="#">Learn More</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


      <section class="probootstrap-section">
        <div class="container">
            <div class="col-md-12 text-center section-heading probootstrap-animate" data-animate-effect="fadeIn">
                <h2>Past Events</h2>
                <p class="lead">The greatness of a community is most accurately measured by the compassionate actions of it's members </div>
            <div class="row probootstrap-gutter10">

                <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
                    <a href="img/14.jpg" class="image-popup"><img src="img/14.jpg" alt="image" class="img-responsive"></a>
                  </div>
                  <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
                      <a href="img/18.jpg" class="image-popup"><img src="img/18.jpg" alt="image" class="img-responsive"></a>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
                        <a href="img/19.jpg" class="image-popup"><img src="img/19.jpg" alt="image" class="img-responsive"></a>
                      </div>
                      <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
                          <a href="img/21.jpg" class="image-popup"><img src="img/21.jpg" alt="image" class="img-responsive"></a>
                        </div>
            </div>
        </div>
      </div>
      
      <section class="probootstrap-section  probootstrap-section-colored">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center section-heading probootstrap-animate">
              <h2>Together we can make a difference</h2>
              
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 probootstrap-animate">
              <div class="owl-carousel owl-carousel-testimony owl-carousel-fullwidth">
                <div class="item">

                  <div class="probootstrap-testimony-wrap text-center">
                    <figure>
                      <img src="img/person_1.jpg" alt="Free Bootstrap Template by uicookies.com">
                    </figure>
                    <blockquote class="quote">&ldquo;Be a rainbow in someone's cloud.&rdquo; <cite class="author"> &mdash; <span>Maya Angelou</span></cite></blockquote>
                  </div>

                </div>
                <div class="item">
                  <div class="probootstrap-testimony-wrap text-center">
                    <figure>
                      <img src="img/person_2.jpg" alt="Free Bootstrap Template by uicookies.com">
                    </figure>
                    <blockquote class="quote">Philanthropy is not about money... It's about feeling the pain of others and caring enough about their needs <cite class="author"> <span></span></cite></blockquote>
                  </div>
                </div>
                <div class="item">
                  <div class="probootstrap-testimony-wrap text-center">
                    <figure>
                      <img src="img/person_3.jpg" alt="Free Bootstrap Template by uicookies.com">
                    </figure>
                    <blockquote class="quote">&ldquo;Smallest adt of kindness is worth more than the grandest intention, giving is the breast of act of grace &rdquo; <cite class="author">&mdash; <span>Oscar Wilde</span></cite></blockquote>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </section>
        
      <section class="probootstrap-half">
        <div class="image">
          <div class="image-bg">
            <img src="img/img_sq_5_big.jpg" alt="Free Bootstrap Template by uicookies.com">
          </div>
        </div>
        <div class="text">
          <div class="probootstrap-animate">
            <h3>Achievements</h3>
            <p>Drug abuse among the youth has become an epidemic of massive proportion in the country 
                we organized reorientation and sensitization programs on drug education among youths. Organized awareness walk on the effects and dangers of drug abuse, engaged them in volunteering activities. 
                Encouraged school enrollment through sponsorship programs thereby reducing the reate of dropout. We visited rehabilitation centers to sensitize, offer counselling services and provide support to the centers.</p>
           
          </div>
        </div>
      </section>

      <section class="probootstrap-section">
        <div class="container">
          <div class="row">
            <div class="col-md-3 probootstrap-animate">
              <h3>News</h3>
              <ul class="probootstrap-news">
                <li>
                  <span class="probootstrap-date">July 16, 2017</span>
                  <h3><a href="{{route('news.index')}}">What is Drugs?</a></h3>
                  <p>Drug is a substance in form of chemical or herbal which has physiological effect. In other words, drug is anything when taken changes the physic logical functions of the body</p>
                  <p><span class="probootstrap-meta"><a href="{{route('news.index')}}"><i class="icon-redo2"></i> 14</a> <a href="#"><i class="icon-bubbles2"></i> 7</a></span></p>
                </li>
                <li>
                  <span class="probootstrap-date">July 16, 2017</span>
                  <h3><a href="{{route('news.index')}}">Club drug</a></h3>
                  <p>The most commonly used club drugs at of publication are ketamine (special K) GHB, methamphetamine and ectasy.</p>
                  <p><span class="probootstrap-meta"><a href="{{route('news.index')}}"><i class="icon-redo2"></i> 14</a> <a href="#"><i class="icon-bubbles2"></i> 7</a></span></p>
                </li>
              </ul>
              <p><a href="{{route('news.index')}}" class="btn btn-primary">View all news</a></p>
            </div>
            <div class="col-md-6 probootstrap-animate">
              <h3>About Us</h3>
              <p><img src="img/new1.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></p>
              <p>
                Enhancing national assets through facilitating community development.Fostering Peace, unity and tolerance in the community.
              </p>
              <p><a href="{{route('about.index')}}" class="btn btn-primary">Learn More</a></p>
            </div>
            <div class="col-md-3 probootstrap-animate">
              <h3>Gallery</h3>
              
              <div class="owl-carousel owl-carousel-fullwidth">
                <div class="item">
                  <a href="img/5.jpg" class="image-popup">
                    <img src="img/5.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                  </a>
                </div>
                <div class="item">
                  <a href="img/6.jpg" class="image-popup">
                    <img src="img/6.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                  </a>
                </div>
                <div class="item">
                  <a href="img/3.jpg" class="image-popup">
                    <img src="img/3.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                  </a>
                </div>
                <div class="item">
                  <a href="img/4.jpg" class="image-popup">
                    <img src="img/4.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                  </a>
                </div>
                <div class="item">
                  <a href="img/17.jpg" class="image-popup">
                    <img src="img/17.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                  </a>
                </div>
                <div class="item">
                  <a href="img/10.jpg" class="image-popup">
                    <img src="img/10.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                  </a>
                </div>
                <div class="item">
                  <a href="img/24.jpg" class="image-popup">
                    <img src="img/24.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                  </a>
                </div>
              </div>

              <p class="text-center"><a href="#" class="btn btn-primary">View all gallery</a></p>

            </div>
          </div>
        </div>
      </section>
@endsection('content')
      

    