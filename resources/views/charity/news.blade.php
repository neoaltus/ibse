@extends('layout.main')  
@section('content')       
      <section class="probootstrap-hero probootstrap-hero-inner" style="background-image: url(img/hero_bg_bw_1.jpg)"  data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="probootstrap-slider-text probootstrap-animate" data-animate-effect="fadeIn">
                <h1 class="probootstrap-heading probootstrap-animate">Learn More About Drug Abuse<span>Together we can make a difference</span></h1>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section">
        <div class="container">
          <div class="row mb40">
            <div class="col-md-6 col-md-push-6 probootstrap-animate">
              <p><img src="img/new1.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></p>
            </div>
            <div class="col-md-5 col-md-pull-6 news-entry probootstrap-animate">
              <h2 class="mb0"><a href="#">What is Drugs?</a></h2>
              <p class="probootstrap-news-date">July 20, 2017 by Admin</p>
              <p>
 Drug is a substance in form of chemical or herbal which has physiological effect. In
other words, drug is anything when taken changes the physic logical functions
      of the body. Drug causes positive effect when used to treat or prevent an illness, and causes
        negative effect when used contrary to its purpose. 
                  
              </p>
              <p><span class="probootstrap-meta-share"><a href="#"><i class="icon-redo2"></i> 14</a> <a href="#"><i class="icon-bubbles2"></i> 7</a></span> <a href="#" class="btn btn-black">Read More...</a></p>
            </div>
          </div>

          <div class="row mb40">
            <div class="col-md-6 probootstrap-animate">
              <p><img src="img/new2.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></p>
            </div>
            <div class="col-md-5 col-md-push-1  news-entry probootstrap-animate">
              <h2 class="mb0"><a href="#">Drug Abuse</a></h2>
              <p class="probootstrap-news-date">July 20, 2017 by Admin</p>
              <p>
                <h3>Drugs can be abused by</h3>
                <ol type="1">
              <li>self prescription </li>                
                 <li>excessive usage</li> 
                <li> unregulated usage etc</li>
                  <li>Both licit and illicit drugs </li>
                </ol>  

              </p>
              <p><span class="probootstrap-meta-share"><a href="#"><i class="icon-redo2"></i> 14</a> <a href="#"><i class="icon-bubbles2"></i> 7</a></span> <a href="#" class="btn btn-black">Read More...</a></p>
            </div>
          </div>

          <div class="row mb40">
            <div class="col-md-6 col-md-push-6 probootstrap-animate">
              <p><img src="img/new1.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></p>
            </div>
            <div class="col-md-5 col-md-pull-6 news-entry probootstrap-animate">
              <h2 class="mb0"><a href="#">What is Drug addiction</a></h2>
              <p class="probootstrap-news-date">July 20, 2017 by Admin</p>
              <p>
                  Drug addiction can be defined as a chronic,brain relapsing disease that is
                  characterized by compulsive drug seeking and use, despite harmful consequences
                  .it is considered a brain disease because drugs changes the brain structure and
                  how it works. It distrups the normal,healthy functioning of the underlying
                  organ,have serious harmful effects. if left untreated , can last a life time. 
                  What guardians need to know about drug
                  
              </p>
              <p>
                  Most commonly used drugs and their effects for many people, the term “drug” tends to suggest
                  illegal activity However, the use of tobacco and alcohol is legal for people over the age of 18 or 19
                  (depending on the province), and medicines are legal when prescribed or
                  available over the counter. Alcohol and tobacco are
                  the drugs that young people are most likely to try, and cannabis is
                  the most widely used illegal drug. The short-term and
                  long-term effects of these drugs are described in the chart below.
                  
              </p>
              <p><span class="probootstrap-meta-share"><a href="#"><i class="icon-redo2"></i> 14</a> <a href="#"><i class="icon-bubbles2"></i> 7</a></span> <a href="#" class="btn btn-black">Read More...</a></p>
            </div>
          </div>

          <div class="row mb40">
            <div class="col-md-6 probootstrap-animate">
              <p><img src="img/new2.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></p>
            </div>
            <div class="col-md-5 col-md-push-1  news-entry probootstrap-animate">
              <h2 class="mb0"><a href="{{route('single.index')}}">Alcohol (depressant)</a></h2>
              <p class="probootstrap-news-date">July 20, 2017 by Admin</p>
              <p>
                  The term “alcohol”
                  refers to substances made by fermenting or distilling grains or fruits; for
                  example, wine, beer and hard liquor. Alcohol is the most popular drug in most
                  provinces of Canada. short-term effects may
                  include depresses the central
                  nervous system (CNS) relaxation and
                  feelings of well-being, but in some cases depression and anger increased activity and decreased inhibition drowsiness,
                  dizziness, slurred speech and loss of coordination and concentration in extreme cases, loss of consciousness and even death long-term effects may include damage to the stomach lining, ulcers, decreased appetite and malnutrition liver damage, cirrhosis of the liver blackouts (periods when the person is conscious and functioning but is later unable to recall what they did or said) brain damage that causes problems with memory, judgment and abstract thinkingincreased risk ofhemorrhagic stroke impotence in men and
                  infertility in women cancer of the mouth
                  and throat, as well as other types of cancer heavy use can lead to
                  dependence. 
                  
                  {{--  <p>Tobacco (stimulant) is usually smoked but can also be chewed. short-term effects may include stimulates the central nervous system (CNS) feelings of pleasure, stimulation and relaxation increased heartbeat,
                  blood pressure and blood sugar irritations of the
                  throat and lungs (coughing) reduced appetite and
                  endurance long-term effects may include heart disease,
                  strokes, emphysema, chronic bronchitis and aneurysms cancer of the lung,
                  throat, stomach,bladder, kidney and pancreas physical and
                  psychological addiction with significant withdrawal symptoms 
                  Cannabis (marijuana) is the most widely used illegal drug in Canada. It is a mood-altering drug that comes from the Cannabis sativa plant. People use it in three forms: marijuana (the dried and ground bud of the plant), hash or hashish oil/weed oil. The cannabis high comes from the chemical THC. The cannabis that was smoked in the sixties and seventies was much less potent than it is today. With the improvement in horticulture and growing techniques, the concentration of THC in cannabis today
                  has increased substantially. short-term effects may include feelings of calmness and relaxation, as well as clumsiness and slowed-down reactions, drowsiness  giddiness, talkativeness or quiet seriousness heightened senses forgetfulness and reduced ability to concentrate distorted sense of
                  space and time increased heart rate and changes in blood pressure increased appetite anxiety, occasionally panic attacks and/ or paranoia (suspicious feelings) long-term effects may include: decreased motivation and interest, as well as difficulties with memory and concentration chronic coughing and lung infections cancer (As a result
                  of the heavy use of pesticides and fertilizer in indoor grow-ops, the tar in
                  cannabis smoke contains much higher amounts of cancer-producing agents
                  than tar in tobacco smoke.)  psychological and
                  physical dependence can occur among heavy or regular users.</p>  --}}
            
            
            </p>
              <p><span class="probootstrap-meta-share"><a href="{{route('single.index')}}"><i class="icon-redo2"></i> 14</a> <a href="{{route('single.index')}}"><i class="icon-bubbles2"></i> 7</a></span> <a href="{{route('single.index')}}" class="btn btn-black">Read More...</a></p>
            </div>
          </div>

          <div class="row mb40">
            <div class="col-md-6 col-md-push-6 probootstrap-animate">
              <p><img src="img/new1.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></p>
            </div>
            <div class="col-md-5 col-md-pull-6 news-entry probootstrap-animate">
              <h2 class="mb0"><a href="#">Less Commonly Used Drug and Their Effects</a></h2>
              <p class="probootstrap-news-date">July 20, 2017 by Admin</p>
              <p>
                  Other illegal drugs,
                  such as those described in the chart below, are much less commonly
                  used by young people in Canada. short-term effects may
                  include stimulating the central nervous system(CNS), feelings of
                  well-being, increased alertness and energy, increased heart rate
                  and breathing rate,increased body temperature. “twitching,” teeth grinding and obsession with performing repetitive tasks such as cleaning,
                  hand-washing or assembling and disassembling objects irritability, insomnia, confusion, hallucinations,anxiety, paranoia (suspicious feelings) and increased aggression long-term effects may include loss of muscle
                  control with symptoms similar to Parkinson’s Disease, severe paranoia (suspicious feelings) and severe depression, black teeth and gums,
                  {{--  ashen skin and repellent body odour.
                  Amphetamines and methamphetamines (stimulants) Amphetamines are a large group of stimulant drugs. They can be powders (ranging in colour from white to brown, or even purple), pills or liquids. Amphetamines stimulate the central nervous system. Their effects are similar; however, the intensity will vary with the drug and the manner in which it is taken.
                  Methamphetamine is a powerful member of the amphetamine family that comes in many different formulations dating back to the late 1880s. The popularity of methamphetamine has come and gone over the years. Recently a new form of methamphetamine has emerged worldwide. Although it is the same drug, it is now much more potent. Methamphetamine comes in powder or crystal form. Crystal meth,a colourless crystalline solid, is a form of methamphetamine that is mostly smoked. Methamphetamine is
                  highly addictive. Addiction to methamphetamine is treatable, although it can be a long process and relapse is common.
                     --}}
                                    


              </p>
              <p><span class="probootstrap-meta-share"><a href="#"><i class="icon-redo2"></i> 14</a> <a href="#"><i class="icon-bubbles2"></i> 7</a></span> <a href="#" class="btn btn-black">Read More...</a></p>
            </div>
          </div>

          <div class="row mb40">
            <div class="col-md-6 probootstrap-animate">
              <p><img src="img/new2.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></p>
            </div>
            <div class="col-md-5 col-md-push-1  news-entry probootstrap-animate">
              <h2 class="mb0"><a href="#">Club Drugs </a></h2>
              <p class="probootstrap-news-date">July 20, 2017 by Admin</p>
              <p>
                  <p>
                      The most commonly used
                      club drugs at the time of publication are ketamine (Special K), GHB,
                      methamphetamine and ecstasy. It is important to know that the so-called club drugs are not just confined to the dance club scene. The dangers of these drugs include a strong likelihood that they have not been properly manufactured, and in the case of GHB and ecstasy, the fact that they cannot be safely used in combination with alcohol.
                      Ketamine is used as an anesthetic in medicine and by veterinarians as a tranquillizer on large animals. It is similar to PCP (angel dust) and creates a dissociative effect, causing the user to experience loss of identity and distorted perception of time. The user can also suffer from hallucinations and exhibit psychotic behaviour.
                      short-term effects may
                      include feelings of relaxation and sedation at higher doses, distorted perceptions, confusion and blackouts long-term effects may include addiction (in high doses) delirium, amnesia, high blood pressure, depression and breathing problems becoming withdrawn,
                      {{--  paranoid and very uncoordinated GHB, a sedative hypnotic originally developed
                      as a sleep aid, is an odourless and colourless liquid mixed in water or provided in the form of a white powder. Effects are felt in about 10 to 20
                      minutes and can last up to four hours. GHB makes the user feel very drunk but is out of the system in 24 to 48 hours. short-term effects may
                      include feelings of relaxation and happiness, and increased sociability (similar to the effects of
                        --}}
                      {{--  alcohol) long-term effects may include (in high doses) dizziness, drowsiness, nausea, vomiting, headaches, loss of consciousness, loss
                      of reflexes, impaired breathing and even death physical dependence withdrawal symptoms
                      such as insomnia, anxiety, tremors and sweating
                      ECSTASY IS A POWERFUL
                      STIMULANT THAT IS CHEMICALLY RELATED TO METHAMPHETAMINE “E,” as it is known, is
                      almost always found in tablet form and is taken orally. Ecstasy has a euphoric
                      effect and raises the energy level of the user, which makes it attractive to the
                      dance culture. It also has some hallucinogenic effects. Ecstasy is sold in
                      coloured tablets that are usually imprinted with some form of caricature or
                      logo. This is a marketing technique used to attract youth. Unfortunately, users
                      may mistakenly view these drugs as being safe because they come in the form of
                      pills or tablets. It is very rare to find a pure ecstasy tablet for sale. It is
                      more common to see other less expensive drugs such as methamphetamine, ketamine or GHB
                      contained in a single tablet. This places the user at risk of a potentially dangerous
                      drug interaction. short-term effects may
                      include feelings of euphoria,
                      pleasure, empathy and sociability, but also confusion, depression, sleep
                      problems, anxiety and panic attacks blurred vision, nausea, muscle tension, teeth grinding faintness, chills,
                      sweating; increased heart rate and blood pressure, elevated body temperature (in high doses) distortions in perception, thinking and memory,as well as hallucinations, death from
                      dehydration and hypothermia in the
                      context of raves or dances long-term effects may include weight loss irreversible brain
                      damage with symptoms similar to the early onset of Alzheimer’s disease · flashbacks, paranoia (suspicious feelings), depression and psychosis (severe mental illness), liver damage.
                      Halucinogens: Hallucinogens,
                      the best known of which is LSD, are drugs that act on the central nervous system to greatly affect the way one feels and thinks. These drugs typically take the form of tablets or capsules containing powder of any colour but can also be
                      in the form of magic mushrooms (psilocybin). short-term effects may
                      include mood swings from
                      euphoria to sadness or fear, and back again changes to the senses (the way you see,
                      hear, taste and touch) and hallucinations (at higher doses) increased heartbeat
                      and blood pressure, dizziness, upset stomach, numbness of the mouth, nausea,
                      anxiety and shivering long-term effects may include flashbacks psychosis (severe
                      mental illness) in vulnerable users possible
                      psychological dependence in chronic users. Solvents/Inhalants Common
                      household products such as quick-drying glues, gasoline, nail polish remover, paint
                      thinner and cleaning fluids contain solvents that are commonly abused. Gas propellants
                      in aerosol products like hairsprays and air fresheners are also inhaled. short-term effects may
                      include euphoria, dizziness,
                      numbness and weightlessness decreased motor
                      coordination, muscle weakness, slowed reflexes, impaired judgment, visual
                      impairment, ringing in the ears bloodshot, watery
                      eyes increased heart rate,
                      irregular heartbeat, headaches sneezing, coughing, nasal inflammation, respiratory depression, nausea, vomiting and diarrhea (if inhaled for a long period of time) coma or seizures, unconsciousness and brain damage death from asphyxiation (suffocation) long-term effects may include psychological problems such as apathy, mood swings, depression and paranoia (suspicious feelings) blood abnormalities and damage to the liver, kidneys, lungs and heart
                      COCAINE (STIMULANT)
                      Cocaine is a fine, white crystalline powder, often diluted with other substances, that is sniffed,
                      smoked or sometimes injected. It comes in two forms:
                      <ol type="1">
                    <li> cocaine hydrochloride a
                      drug that is sniffed in powder form, and</li>
                       <li> crack cocaine, a cocaine-based
                      substance that is smoked. short-term effects may include a sense of
    
                      excitement, extra energy and confidence, and an elevated tolerance for pain decreased appetite,
                      dilated pupils, sweating and paleness increased heartbeat and breathing long-term effects may include chronic snorting causes runny or bleeding noses and holes in the barrier separating the nostrils, depression, restlessness, sleeping, eating and sexual problem, chronic, heavy cocaine use can cause severe psychiatric disorders. </li>
                    </ol>
                      Heroin (depressant) Heroin
                      is a drug derived from morphine, which is derived from the opium poppy. It is most often injected but can also be sniffed, smoked or swallowed. short-term effects may include an intense feeling of well-being or euphoria, numbness and pain relief  nausea, vomiting and severe itching “nodding” or alternating between a wakeful and drowsy state  overdose is a common
                      cause of death long-term effects may include collapsed veins and risk of contracting hepatitis, HIV and other infections from the use of needles, malnutrition, chronic constipation,  addiction can lead to serious personal problems often involving crime, theft and poverty.
                      
                        --}}
                  </p>
                        
              </p>
              <p><span class="probootstrap-meta-share"><a href="#"><i class="icon-redo2"></i> 14</a> <a href="#"><i class="icon-bubbles2"></i> 7</a></span> <a href="#" class="btn btn-black">Read More...</a></p>
            </div>
          </div>

       


        </div>
      </section>

@endsection      

