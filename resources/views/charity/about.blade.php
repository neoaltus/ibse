      @extends('layout.main')
      @section('content')
      <section class="probootstrap-hero probootstrap-hero-inner" style="background-image: url(img/images2.jpeg)"  data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="probootstrap-slider-text probootstrap-animate" data-animate-effect="fadeIn">
                <h1 class="probootstrap-heading probootstrap-animate">About Us <span>Together we can make a difference</span></h1>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section">
        <div class="container">
          <div class="row mb40">
            <div class="col-md-6 col-md-push-6 probootstrap-animate">
              <p><img src="img/img_sq_1.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></p>
            </div>
            <div class="col-md-5 col-md-pull-6 probootstrap-animate">
              <h2>Our Story</h2>
              <p>Recognising that countries in Africa especially communities in Nigeria, are faced with a lot of societal challenges like talking about drugs and it’s abuse in the streets, at school, on the internet and TV. Some of it is true, some not. Much of what hear about drugs actually comes from the those selling them. Reformed drug dealers have confessed they would have said anything to get others to buy drugs.
                 Drugs have been part of our culture since the middle of the last century. Popularized in the 1960s by music and mass media, they invade all aspects of society.
                An estimated 208 million people internationally consume illegal drugs. In Nigeria, results from the United Nations reported only 925 people received treatment in 2004. With a total population of 149 million in Nigeria, it is very likely that there are thousands of people who are addicted to illicit drugs but are not receiving treatment. The use of hard drugs, especially among the youth, has become a real social menace and cuts across all social strata, with children from both rich 
                and poor backgrounds deeply into it, a trend that all hands must be on deck to change if we are to save our upcoming generation from ruin.  wayward attitudes of youths,
                 poor level of education and enlightenment, idleness, poverty in many homes, drug addiction and violence; recognizing the need for proactive action in growing the capacity of the people, enhancing community assets of all kinds, securing essential services and effective community-led planning and stronger local governance.
                </p>
         <p>
IN RESPONSE to these challenges, concerned Nigerians and intellectuals of like minds came together in June 2018 in Abuja and resolved:
To form a non-profit and non-political organization that will provide a platform for the people of Nigeria to come together and discuss problems affecting them, proffer and implement solutions. Facilitating community reorientation and sensitization programs on drug education, sale and abuse of illicit drugs. Introduce drug free clubs in both government and private schools in Nigeria. Encourage school enrollment and reduce the rate of dropouts in Nigeria. Enhancing national assets. Empower the communities by encouraging, community works, skills acquisition and training. Eradicate extreme poverty and reduce the level of unemployment through loan schemes, grants and establishment of small scale businesses. To facilitate community development through partnership with registered groups including international organizations and agencies.
 To foster peace, unity and tolerance in our communities to enable speedy development.
              
 </p></div>
            
          </div>

          <div class="row">
            <div class="col-md-6 probootstrap-animate">
              <p><img src="img/img_sq_2.jpg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive"></p>
            </div>
            <div class="col-md-5 col-md-push-1 probootstrap-animate">
              <h2>Our Mission</h2>
              <p>We are reach out to the people, we come together and discuss problems affecting them, proffer solutions and implement those solutions</p>
              <p>To foster peace, unity and tolerance in our communities can help in development of the community.</p>
            </div>
            
          </div>

        </div>
      </section>

      <section class="probootstrap-section probootstrap-border-top">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center section-heading probootstrap-animate" data-animate-effect="fadeIn">
              <h2>The Team</h2>
              <p class="lead">
                It's amazing what you can accomplish with a team with common vision
              </p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6 probootstrap-animate">
              <a href="#" class="probootstrap-team">
                <img src="img/images.jpeg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                <div class="probootstrap-team-info">
                  {{--  <h3>Jeremy Slater <span class="position">Partner/CEO</span></h3>  --}}
                </div>
              </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 probootstrap-animate">
              <a href="#" class="probootstrap-team">
                <img src="img/images.jpeg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                <div class="probootstrap-team-info">
                  {{--  <h3>James Butterly <span class="position">Partner/CTO</span></h3>  --}}
                </div>
              </a>
            </div>
            
            <div class="clearfix visible-sm-block visible-xs-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-6 probootstrap-animate">
              <a href="#" class="probootstrap-team">
                <img src="img/images.jpeg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                <div class="probootstrap-team-info">
                  {{--  <h3>Matthew Smith <span class="position">Organizer</span></h3>  --}}
                </div>
              </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 probootstrap-animate">
              <a href="#" class="probootstrap-team">
                <img src="img/images.jpeg" alt="Free Bootstrap Template by uicookies.com" class="img-responsive">
                <div class="probootstrap-team-info">
                  {{--  <h3>Ivan Dubovtsev <span class="position">Organizer</span></h3>  --}}
                  
                </div>
              </a>
            </div>

            <div class="clearfix visible-sm-block visible-xs-block"></div>
          </div>
        </div>
      </section>

      <section class="probootstrap-half">
        <div class="image">
          <div class="image-bg">
            <img src="img/img_sq_5_big.jpg" alt="Free Bootstrap Template by uicookies.com">
          </div>
        </div>
        <div class="text">
          <div class="probootstrap-animate">
            <h3>Achievements</h3>
            <p>Drug abuse among the youth has become an epidemic of massive proportion in the country 
                we organized reorientation and sensitization programs on drug education among youths. Organized awareness walk on the effects and dangers of drug abuse, engaged them in volunteering activities. 
                Encouraged school enrollment through sponsorship programs thereby reducing the reate of dropout. We visited rehabilitation centers to sensitize, offer counselling services and provide support to the centers.</p>
            
          </div>
        </div>
      </section>
      @endsection

      