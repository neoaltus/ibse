@extends('layout.main')  
@section('content')       
      <section class="probootstrap-hero probootstrap-hero-inner" style="background-image: url(img/hero_bg_bw_1.jpg)"  data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="probootstrap-slider-text probootstrap-animate" data-animate-effect="fadeIn">
                <h1 class="probootstrap-heading probootstrap-animate">Our Gallery </div>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section">
        <div class="container">
          <h1>Support and donations to rehabilitatoin centers in Kebbi</h1>
          <div class="row probootstrap-gutter10">
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/1.jpg" class="image-popup"><img src="img/1.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/2.jpg" class="image-popup"><img src="img/2.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/3.jpg" class="image-popup"><img src="img/3.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/4.jpg" class="image-popup"><img src="img/4.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/5.jpg" class="image-popup"><img src="img/5.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/6.jpg" class="image-popup"><img src="img/6.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/7.jpg" class="image-popup"><img src="img/7.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/10.jpg" class="image-popup"><img src="img/10.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/17.jpg" class="image-popup"><img src="img/17.jpg" alt="image" class="img-responsive"></a>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/11.jpg" class="image-popup"><img src="img/11.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/15.jpg" class="image-popup"><img src="img/15.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/9.jpg" class="image-popup"><img src="img/9.jpg" alt="image" class="img-responsive"></a>
            </div>
            
            
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/8.jpg" class="image-popup"><img src="img/8.jpg" alt="image" class="img-responsive"></a>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/12.jpg" class="image-popup"><img src="img/12.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/13.jpg" class="image-popup"><img src="img/13.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/16.jpg" class="image-popup"><img src="img/16.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/14.jpg" class="image-popup"><img src="img/14.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/18.jpg" class="image-popup"><img src="img/18.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/19.jpg" class="image-popup"><img src="img/19.jpg" alt="image" class="img-responsive"></a>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/21.jpg" class="image-popup"><img src="img/21.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/22.jpg" class="image-popup"><img src="img/22.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/23.jpg" class="image-popup"><img src="img/23.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/26.jpg" class="image-popup"><img src="img/26.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/27.jpg" class="image-popup"><img src="img/27.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/24.jpg" class="image-popup"><img src="img/24.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/20.jpg" class="image-popup"><img src="img/20.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/25.jpg" class="image-popup"><img src="img/25.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/31.jpg" class="image-popup"><img src="img/31.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/28.jpg" class="image-popup"><img src="img/28.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/29.jpg" class="image-popup"><img src="img/29.jpg" alt="image" class="img-responsive"></a>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/30.jpg" class="image-popup"><img src="img/30.jpg" alt="image" class="img-responsive"></a>
            </div>

            {{--  <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/31.jpg" class="image-popup"><img src="img/31.jpg" alt="image" class="img-responsive"></a>
            </div>    --}}
            {{--  <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/33.jpg" class="image-popup"><img src="img/33.jpg" alt="image" class="img-responsive"></a>
            </div>--}}
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate">
              <a href="img/34.jpg" class="image-popup"><img src="img/34.jpg" alt="image" class="img-responsive"></a>
            </div> 
           <h2>IBSE drug abuse awareness walk</h2>
           <div class="col-md-6 col-sm-6 col-xs-6 gal-item probootstrap-animate">
            <a href="img/a.jpg" class="image-popup"><img src="img/a.jpg" alt="image" class="img-responsive"></a>
          </div>   
          <div class="col-md-6 col-sm-6 col-xs-6 gal-item probootstrap-animate">
            <a href="img/b.jpg" class="image-popup"><img src="img/b.jpg" alt="image" class="img-responsive"></a>
          </div>  
         <h2>Meeting with Government stakeholders on drug abuse</h2>
          <div class="col-md-4 col-sm-4 col-xs-4 gal-item probootstrap-animate">
            <a href="img/d.jpg" class="image-popup"><img src="img/d.jpg" alt="image" class="img-responsive"></a>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-4 gal-item probootstrap-animate">
            <a href="img/e.jpg" class="image-popup"><img src="img/e.jpg" alt="image" class="img-responsive"></a>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-4 gal-item probootstrap-animate">
            <a href="img/f.jpg" class="image-popup"><img src="img/f.jpg" alt="image" class="img-responsive"></a>
          </div>

          </div>
        </div>
      </section>
      
@endsection