@extends('layout.main')
@section('content')
      
      <section class="probootstrap-hero probootstrap-hero-inner" style="background-image: url(img/hero_bg_bw_1.jpg)"  data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="probootstrap-slider-text probootstrap-animate" data-animate-effect="fadeIn">
                <h1 class="probootstrap-heading probootstrap-animate">Donate <span>We give cheerfully and accept gratefully</span></h1>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section">
        <div class="container">
          <div class="row">
          <div class="col-md-12 probootstrap-animate">
            <form action="#" method="post" class="probootstrap-form">
              <div class="form-group">
                <label for="name">Full Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="eg. John Doe">
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="eg. info@uicookies.com">
              </div>
              <div class="form-group">
                <label for="amount">Amount</label>
                <input type="text" class="form-control" id="amount" name="amount" placeholder="$1000.00">
              </div>
              <div class="form-group">
                <label for="message">Note</label>
                <textarea cols="30" rows="10" class="form-control" id="message" name="message" placeholder="eg. This donation is for the children who needs food."></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-primary btn-lg" id="submit" name="submit" value="Donate">
              </div>
            </form>
          </div>

        
          
        </div>
        </div>
      </section>
      

 @endsection